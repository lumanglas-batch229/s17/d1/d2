//console.log("Hello World!")


// [Section] - Functions
//Functions in JS are lines/blocks of codes that will tell device/application to perform a particular task.
// Functions are mostly created to create complicated task to run several lines of code succession.
// They are also used to prevent repeating line of codes.
// Function Decleration -> "Function"
/*
Syntax:
function functionName(){
	//code block
	let number = 10;
	console.log(number)
}

console.log(number) -> error should be called inside the braces
*/

function printGrade(){
	let grade1 = 90;
	let grade2 = 95;
	let avg = (grade1 + grade2)/2;
	console.log(avg);
}

// Calling or invokation
printGrade();


// [Section] Function Invocation
//Invocation will execute block of codes inside the function being called.

//This will call function named printGrade()
printGrade();

//sampleInvocation(); -> undeclared function cannot be invoke.

//[Section] Function Decleration vs Expression

function declaredFunction() {
	console.log("Hello World from declaredFunction()")
}
declaredFunction();


// This is Function expression
//A Function can also be stored in a variable. This is called function expression.

// variableFunction();
/*
 error - function expressions being stored in a let or const, cannot be hoisted.

*/
// Anonymous Function
// Function Expression Example
let variableFunction = function() {
	console.log("Hello Again!");
}
variableFunction();

let functionExpression = function funcName() {
	console.log("Hello from the other side");
}
functionExpression(); //This is the right invokation in function expression
// funcName(); -> this will return error


// You can re-assign declared function and function expression to a new anonymous function

declaredFunction = function() {
	console.log("Updated Declared Function()");
}
declaredFunction();

functionExpression = function() {
	console.log("updated functionExpression()");
}
functionExpression();

// However we cannot re-assign a function expression initialized with const.

const constantFunction = function() {
	console.log("Initialized with const!")
}
constantFunction();

/*constantFunction = function(){
	console.log("Will try re-assign");
}
constantFunction();
*/

// [SECTION] - Function Scoping
/*
1. Local/block scope
2. Global Scope
3. Function Scope
*/

{
	// This is a local variable and its value is only accessible inside the curly braces.
	let localVariable = "Armando Perez";
}
// This is a Global Variable its value is accessible in the code base.
let globalVariable = "Mr.WorldWide";
console.log(globalVariable);
// console.log(localVariable); -> will return error

function showNames(){
	//function scope variables
	var functionVar = "Joe";
	const functionConst = "John";
	let functionLet = "Jane";

	console.log(functionVar)
	console.log(functionConst)
	console.log(functionLet)
}

showNames();
/*
console.log(functionVar)
console.log(functionConst)
console.log(functionLet)

This will return error since the 3 variable are stored in the Function
*/

// Nested Function

function myNewFunction() {
	// Globale variable inside this function
	let name = "Jane Doe";

	function nestedFunction() {
		let nestedName = "Johnny Depp";
		console.log(nestedName);
		console.log(name);
	}
	nestedFunction();
	// console.log(nestedName); error/function scoped	
}
myNewFunction();
// nestedFunction(); This will result error

// Function and global scoped variables

let globalName = "Alexandro";

function myNewFunction2() {
	let nameInside = "Renz";
	console.log(globalName);
}
myNewFunction2();
// console.log(nameInside); -> will return error due to variable is inside the {}.

//[SECTION] - Using Alert
// alert() allows us to show small window at the top of our browser.

//alert("Hello World");

function showSampleAlert(){
	alert("Hello User!")
	
}
showSampleAlert();
console.log("I will only log in the console when alert is dismissed")

//Notes on the use of alert ()
// show only alert () for short dialog message.
// Do not overuse alert() because program.js has to wait for it to be dismissed before continuing.

//[SECTION] - Using promt ()
// prompt() allows us to show a small window and gather input
// Usually promt are stored in a variable.
// let samplePrompt = prompt("Enter your name.");

// console.log("Hello, " + samplePrompt);
// console.log(typeof samplePrompt);


let sampleNullPrompt = prompt("Do not enter anything.");
console.log(sampleNullPrompt);

function printWelcomeMessage() {
	let firstName = prompt("Enter your First Name :");
	let lastName = prompt("Enter your Last Name :");

	console.log("Hello, " + firstName + " " + lastName + "!");
	console.log("Welcome to my page!");
}

printWelcomeMessage ();

// [SECTION] - Function Naming Convention
// Function names should be definitive of the task it will perform. It usally contains verb.

function getCourse() {
	let courses = ["Science 101", "Math 101", "English 101"];
	console.log(courses);
}
getCourse();

//Avoid generic names to avoid confusion within your code.

function get() {
	let name = "Jamie";
	console.log (name);

}
get ();